import * as d3 from 'd3';
import type { GradientScale } from '@/types/GradientScale';

function createColorScale(domainScale: Array<Number>, rangeScale: Array<String>) {
  const minDomain = domainScale[0];
  const maxDomain = domainScale[1];

  const minRange: any = rangeScale[0];
  const maxRange: any = rangeScale[1];

  const colorScale = d3.scaleLinear()
    .domain([minDomain, maxDomain])
    .range([minRange, maxRange]);

  return colorScale;
}

function createColorCaption(gradientObject: GradientScale) {
  const minColor = gradientObject['min'].color;
  const maxColor = gradientObject['max'].color;

  return 'linear-gradient(to right, ' + minColor + ' 0%, ' + maxColor + ' 100%)';
}

function getColorScale(gradientObject: GradientScale) {
  let colorDomain: any = [];
  let colorRange: any = [];

  colorDomain.push(gradientObject['min'].value);
  colorRange.push(gradientObject['min'].color);

  const strokeList = [] as any[];

  Object.keys(gradientObject).forEach((key) => {
    if (key !== 'min' && key !== 'max') {
      const stroke = gradientObject[key];
      const value = stroke.value;
      const color = stroke.color;
      const strokeData = [value, color];
      strokeList.push(strokeData);
    }
  });

  if (strokeList.length > 0) {
    strokeList.sort((a, b) => {
      return a[0] - b[0];
    });

    for (let i = 0; i < strokeList.length; i++) {
      colorDomain.push(strokeList[i][0]);
      colorRange.push(strokeList[i][1]);
    }
  }

  colorDomain.push(gradientObject['max'].value);
  colorRange.push(gradientObject['max'].color);

  const colorScale = d3.scaleLinear()
    .domain(colorDomain)
    .range(colorRange);

  return colorScale;
}

function updateColorCaption(gradientObject: GradientScale) {
  const minColor = gradientObject['min'].color;
  const maxColor = gradientObject['max'].color;

  const minValue: any = gradientObject['min'].value;
  const maxValue: any = gradientObject['max'].value;

  const delta = maxValue - minValue;

  let linearColorScale = 'linear-gradient(to right, '+minColor+' 0%, ';

  let strokeList = [] as any[];

  Object.keys(gradientObject).forEach((key) => {
    if (key !== 'min' && key !== 'max') {
      const color = gradientObject[key].color;
      const value: any = gradientObject[key].value;
      const percent = (value - minValue) * 100 / delta;
      const stroke = [color, percent];
      strokeList.push(stroke);
    }
  });

  if (strokeList.length > 0) {
    strokeList.sort((a, b) => {
      return a[1] - b[1];
    });

    for (let i = 0; i < strokeList.length; i++) {
      linearColorScale += strokeList[i][0] + ' ' + strokeList[i][1] + '%, ';
    }
  }

  linearColorScale += maxColor + ' 100%)';

  return linearColorScale;
}

function processData(gradient: GradientScale) {
  const propsGradient = {} as GradientScale;

  Object.keys(gradient).forEach((key) => {
    const stroke = gradient[key];
    if (key === 'min') {
      propsGradient[key] = {
        color: gradient[key].color,
        value: gradient[key].value,
        name: key,
        x: '5',
        y: '0',
        draggable: false
      }
    } if (key === 'max') {
      propsGradient[key] = {
        color: gradient[key].color,
        value: gradient[key].value,
        name: key,
        x: '468',
        y: '0',
        draggable: false
      }
    }
  });

  return propsGradient;
}

function xToValue(xValue: number, maxValue: number, minValue: number) {
  const delta = maxValue - minValue;
  let xMap = xValue * delta / 468;

  xMap += minValue;

  return xMap;
}

function valueToX(mappingValue: number, maxValue: number, minValue: number) {
  const delta = maxValue - minValue
  const xValue = mappingValue * 468 / delta;

  return xValue;
}

export { createColorScale, getColorScale, createColorCaption, updateColorCaption, processData, xToValue, valueToX };